﻿using UnityEngine;
using System.Collections;

public class SimonRunOutBirthdayScript : MonoBehaviour {

	private float _timer;
	public float TimeToRunOut = 27;
	public string SceneToLoad = "Kitsilano";

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.x>30)
		{
			Application.LoadLevel(SceneToLoad);
		}
	}

	void FixedUpdate()
	{
		_timer += Time.fixedDeltaTime;
		if(_timer > TimeToRunOut)
		{
			rigidbody2D.AddForce(Vector2.right * 30);
		}
	}
}
