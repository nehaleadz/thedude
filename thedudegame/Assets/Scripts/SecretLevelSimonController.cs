﻿using UnityEngine;
using System.Collections;

public class SecretLevelSimonController : MonoBehaviour {


	public Vector2 speed=new Vector2(10,0);
	private Vector3 _localScale;

	// Use this for initialization
	void Start () {
		_localScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () 
	{
		float inputX=Input.GetAxis("Horizontal");
		if(inputX != 0)
		{
			rigidbody2D.velocity = inputX * speed;
		}
		else
		{
			inputX = Input.acceleration.x;
			//give acceleration a bit of tolerance so char doesn't flip like crazy
			if(Mathf.Abs(inputX)>0.05f)
			{
				//make acceleration 4X more sensitive
				inputX *= 4f;

				//acceleration can only go up to 1 (makes it consistant with keyboard input)
				if(inputX>1)
					inputX = 1;
				if(inputX<-1)
					inputX = -1;
				
				rigidbody2D.velocity = speed * inputX;
			}
		}

		//1. flip character sprite on x-axis if horizontal input is negative.
		//2. giving a bit of tolerance so when input is 0, character doesn't flip to right
		if (inputX < -0.1) 
		{
			transform.localScale = new Vector3 (-_localScale.x, _localScale.y, _localScale.z);
		}
		else if(inputX > 0.1)
		{
			transform.localScale = new Vector3 (_localScale.x, _localScale.y, _localScale.z);
		}
	}
}
