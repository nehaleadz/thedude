﻿using UnityEngine;
using System.Collections;

public class GenerateBadGuys : MonoBehaviour {

	public GameObject badGuyTemplate;
	private float generate_ticker = 0;
	public float generate_frequency = 5;

	public float UpperBoundY = 10;
	public float LowerBoundY = 0;

	public float UpperBoundX = 10;
	public float LowerBoundX = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		generate_ticker += Time.deltaTime;
		GenerateBadGuy ();
	}

	private void GenerateBadGuy()
	{
		if(generate_ticker > generate_frequency)
		{
			generate_ticker = 0;
			GameObject badGuy = Instantiate(badGuyTemplate) as GameObject;

			MoveBadGuys moveDirection = badGuy.GetComponent <MoveBadGuys> ();

			if (UpperBoundX<0)
			{
				moveDirection.direction = new Vector3(1,0, 0);
			}
			else 
			{
				moveDirection.direction = new Vector3(-1,0, 0);
			}

			badGuy.transform.position = new Vector2(Random.Range(UpperBoundX, LowerBoundX), Random.Range(UpperBoundY, LowerBoundY));
		}
	}

}
