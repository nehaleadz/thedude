﻿using UnityEngine;
using System.Collections;

public class SceneFade : MonoBehaviour {

	public float fadeSpeed=1.5f;
	private bool sceneStarting = true;

	public Texture endGameTexture;

	void Awake(){
		//we can't have this when transitioning from birthday scene.
		//temporarily taking it out
		//guiTexture.pixelInset = new Rect (0f, 0f, Screen.width, Screen.height);
	}

	void Update(){
		if (sceneStarting) {
			StartScene();
		}
	}

	void FadeToColor(){
		guiTexture.color = Color.Lerp (guiTexture.color, Color.clear, fadeSpeed * Time.deltaTime);
	}

	void FadeToBlack(){
		guiTexture.color = Color.Lerp (guiTexture.color, Color.gray, fadeSpeed  * Time.deltaTime);
	}

	public void StartScene(){
		FadeToColor ();
		if (guiTexture.color.a <= 0.05f) {
			guiTexture.color=Color.clear;
			guiTexture.enabled=false;
			sceneStarting = false;
		}
	}
	public void EndScene(string scene){
		guiTexture.texture=endGameTexture;
		guiTexture.enabled = true;
		FadeToBlack ();
		Application.LoadLevel(scene);
	}
}
