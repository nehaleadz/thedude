﻿using UnityEngine;
using System.Collections;

public class SimonCollision : MonoBehaviour {

	public int score = 0;
	public int Health = 5;
	public GUISkin guiSkin;

	private float hitTimeTicker = 0;
	public float hitTimer = 1.5f;
	private int hitEric=0;

	private ShakeCamera cameraShake;
	private SceneFade sceneFadeScript;

	void Start()
	{
		cameraShake = GameObject.FindObjectOfType (typeof(ShakeCamera)) as ShakeCamera;
		sceneFadeScript = GameObject.FindObjectOfType (typeof(SceneFade)) as SceneFade;
	}
	void Update()
	{
		BlinkingSimon ();
	}

	void OnTriggerEnter2D( Collider2D item ) {

		if (item.gameObject.tag == "Good") {
			score += 5;
		}
		else if (item.gameObject.tag=="Eric") {
			if (!audio.isPlaying) {
				this.gameObject.audio.enabled=true;
				audio.Play ();
			}
			AdjustHealth(-1);
			hitEric++;
			cameraShake.Shake ();
			if (hitEric==3) 
			{
				hitEric=0;
				GoToSecretLevel();
			}
		}
		else {
			AdjustHealth(-1);
		}
		Destroy(item.gameObject);

	}

	void OnGUI() {
		GUI.skin = guiSkin;
		GUI.Label (new Rect (10, 1, 150, 50), "Score: " + score);
	}

	
	void AdjustHealth(int addHealth)
	{
		Health += addHealth;

		isHit = true;

		if(Health <= 0 )
		{
			GameOver();
			Destroy(this.gameObject);
		}
	}

	private bool isHit = false;
	void BlinkingSimon()
	{
		if(isHit)
		{
			hitTimeTicker += Time.deltaTime;
			if(hitTimeTicker > hitTimer)
			{
				hitTimeTicker = 0;
				isHit = false;
			}
			float period = hitTimeTicker / hitTimer;
			period = period * 40;
			float heightRatio = 2/0.4f;
			float heightMoveUp = 0.6f;
			float colorManipulation = (Mathf.Sin (period) / heightRatio) + heightMoveUp;
			Color cc = this.renderer.material.color;
			this.renderer.material.color = new Color(cc.r, cc.g, cc.b, colorManipulation) ;
		}
		else
		{
			this.renderer.material.color = new Color(1f, 1f, 1f);
		}
	}

	void GameOver()
	{
		GameObject go = GameObject.Find ("ItemGenerator");
		ObjectGeneratorScript objGeneratorScript = go.GetComponent <ObjectGeneratorScript> ();
		objGeneratorScript.isGameOver = true;
	}

	void GoToSecretLevel()
	{
		Debug.Log ("Go to secret level ");
		sceneFadeScript.EndScene ("SecretLevel");
	}
}
