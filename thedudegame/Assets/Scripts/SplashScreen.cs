﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour {

	public GUISkin customSkin;

	public void OnGUI(){
		if (customSkin != null) {
			GUI.skin=customSkin;
		}
		int buttonWidth = 650;
		int buttonHeight = 155;
		int halfButtonWidth = buttonWidth / 2;
		int halfScreenWidth = Screen.width / 2;

		if (GUI.Button (new Rect(halfScreenWidth-halfButtonWidth,560,buttonWidth,buttonHeight),"")){
			Application.LoadLevel("Kitsilano");
		}
	}
}
