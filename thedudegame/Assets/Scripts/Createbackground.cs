﻿using UnityEngine;
using System.Collections;

public class Createbackground : MonoBehaviour {

	public float speed=5;

	public GameObject kitsMountains;
	public GameObject kitsMidGround;
	public GameObject kitsForeground;

	GameObject bgMountains;
	GameObject bgMidGround;
	GameObject bgForeground;

	private float backgroundSpawnPosX = 2;

	private bool showKitsSign = true;
	public GameObject KitsSign;
	
	// Use this for initialization
	void Start () {
		bgMountains = Instantiate(kitsMountains) as GameObject;
		bgMountains.transform.position = new Vector2(backgroundSpawnPosX , bgMountains.transform.position.y);

		bgMidGround = Instantiate(kitsMidGround) as GameObject;
		bgMidGround.transform.position = new Vector2 (backgroundSpawnPosX, bgMidGround.transform.position.y);

		bgForeground = Instantiate(kitsForeground) as GameObject;
		bgForeground.transform.position = new Vector2 (backgroundSpawnPosX - bgForeground.renderer.bounds.size.x, kitsForeground.transform.position.y);

		bgForeground = Instantiate(kitsForeground) as GameObject;
		bgForeground.transform.position = new Vector2 (backgroundSpawnPosX, kitsForeground.transform.position.y);
	}
	
	// Update is called once per frame
	void Update () {

		if (bgMountains.transform.position.x < backgroundSpawnPosX) {
			bgMountains = Instantiate(kitsMountains) as GameObject;
			bgMountains.transform.position = new Vector2(backgroundSpawnPosX-speed*Time.deltaTime*2 +kitsMountains.renderer.bounds.size.x , kitsMountains.transform.position.y);
		}
		if (bgMidGround.transform.position.x < backgroundSpawnPosX) {
			bgMidGround = Instantiate(kitsMidGround) as GameObject;
			bgMidGround.transform.position = new Vector2(backgroundSpawnPosX-speed*Time.deltaTime*2 +kitsMidGround.renderer.bounds.size.x , kitsMidGround.transform.position.y);
		}
		if (bgForeground.transform.position.x < backgroundSpawnPosX) {

			bgForeground = Instantiate(kitsForeground) as GameObject;
			bgForeground.transform.position = new Vector2(backgroundSpawnPosX-speed*Time.deltaTime*2 +kitsForeground.renderer.bounds.size.x , kitsForeground.transform.position.y);
		
			if(showKitsSign)
			{
				GameObject kitsSign = Instantiate(KitsSign) as GameObject;
				kitsSign.transform.parent = bgForeground.transform;
				kitsSign.transform.localPosition = new Vector3(0.6407642f, -0.2966259f, 0);
				showKitsSign = false;
			}
		}
	}

}
//Use the following code if we need to change background according to player health.
/*
void Update () {
	
	if (bgMountains.transform.position.x < 5) {
		
		//Find Health
		GameObject go = GameObject.Find ("SimonRun");
		float health = 0;
		if(go!=null)
		{
			SimonCollision speedController = go.GetComponent <SimonCollision> ();
			health = speedController.Health;
		}
		if (health>7){
			bgMountains = Instantiate(kitsMountains) as GameObject;
		}
		else if (health>4 && health<=7){
			bgMountains = Instantiate(kitsBurnabyBgTemplate) as GameObject;
		}
		else if (health<=4){
			bgMountains = Instantiate(burnabyBgTemplate) as GameObject;
		}
		
		bgMountains.transform.position = new Vector2(5-speed*Time.deltaTime*2 +bgMountains.renderer.bounds.size.x , bgMountains.transform.position.y);
	}
*/