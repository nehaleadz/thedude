﻿using UnityEngine;
using System.Collections;

public class BounceOffScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Player")
		{
			rigidbody2D.gravityScale = 5f;
			rigidbody2D.velocity = new Vector2 (10f, 10f);
		}
	}
}
