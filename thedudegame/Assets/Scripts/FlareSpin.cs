﻿using UnityEngine;
using System.Collections;

public class FlareSpin : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
	
	}
	
	public float smooth = 2.0F;
	public float tiltAngle = 30.0F;
	void Update() {
		float tiltAroundZ = Input.acceleration.x * tiltAngle;
		Quaternion target = Quaternion.Euler(0, 0, tiltAroundZ);
		transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
	}
}
