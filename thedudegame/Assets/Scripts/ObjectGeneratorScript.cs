﻿using UnityEngine;
using System.Collections;

public class ObjectGeneratorScript : MonoBehaviour {

	public GameObject GoodItem1;
	public GameObject GoodItem2;
	public GameObject GoodItem3;
	public GameObject GoodItem4;
	public GameObject GoodItem5;
	public GameObject GoodItem6;
	public GameObject GoodItem7;

	public GameObject BadItem1;
	public GameObject BadItem2;
	public GameObject BadItem3;

	public GameObject EricTemplate;

	private GameObject[] goodObjects;
	private int numGoodGameObjects=7;

	private GameObject[] badObjects;
	private int numBadGameObjects=3;

	public float UpperBoundY = 5;
	public float LowerBoundY = 0;

	public float GenerationPositionX = 20;

	public float Eric_generate_frequency = 15; //in seconds
	private float eric_generate_ticker = 0;

	private float item_generate_ticker = 0; //Bad Item generate Ticker
	public float Item_generate_frequency = 3; //in seconds 
	public float Item_gen_freq_max = 5;

	private float good_item_generate_ticker = 0; 
	public float Good_Item_generate_frequency = 1f; //in seconds
	public float Good_Item_gen_freq_max = 3f;
	private float good_item_position_switch_ticker = 0;
	public float Good_Item_position_switch_frequency = 2;

	private Vector3 good_item_startY;
	private Vector3 good_item_targetY;

	private float targetY;

	private SceneFade sceneFadeScript;

	// Use this for initialization
	void Start () {
		good_item_startY = new Vector3 (0, Random.Range (UpperBoundY, LowerBoundY), 0);
		goodObjects = new GameObject[numGoodGameObjects];
		goodObjects [0] = GoodItem1;
		goodObjects [1] = GoodItem2;
		goodObjects [2] = GoodItem3;
		goodObjects [3] = GoodItem4;
		goodObjects [4] = GoodItem5;
		goodObjects [5] = GoodItem6;
		goodObjects [6] = GoodItem7;

		badObjects = new GameObject[numBadGameObjects];
		badObjects [0] = BadItem1;
		badObjects [1] = BadItem2;
		badObjects [2] = BadItem3;

		sceneFadeScript = GameObject.FindObjectOfType (typeof(SceneFade)) as SceneFade;
	}
	
	// Update is called once per frame
	void Update () {
		item_generate_ticker += Time.deltaTime;
		good_item_generate_ticker += Time.deltaTime;
		good_item_position_switch_ticker += Time.deltaTime;
		eric_generate_ticker += Time.deltaTime;

		GenerateItem ();
		GenerateEric ();
		if(isGameOver)
			GameOver();
	}

	private void GenerateEric()
	{
		if(eric_generate_ticker > Eric_generate_frequency)
		{
			eric_generate_ticker = 0;
			GameObject eric = Instantiate(EricTemplate) as GameObject;
		}
	}

	private void GenerateItem()
	{

		if(item_generate_ticker > Item_generate_frequency)
		{
			//randomize item generation by the range;
			float range = Item_gen_freq_max - Item_generate_frequency;
			float tickerInit = Random.Range(-range, 0);

			item_generate_ticker = tickerInit;

			int randomGameObject=Random.Range(0,numBadGameObjects);

			GameObject badItem = Instantiate(badObjects[randomGameObject]) as GameObject;
			badItem.transform.position = new Vector2(GenerationPositionX, Random.Range(UpperBoundY, LowerBoundY));

		}

		if(good_item_position_switch_ticker > Good_Item_position_switch_frequency)
		{
			//randomize item generation by the range;
			float range = Good_Item_gen_freq_max - Good_Item_generate_frequency;
			float tickerInit = Random.Range(-range, 0);

			good_item_position_switch_ticker = tickerInit;
			good_item_startY = good_item_targetY;

			//generate new targetY
			good_item_targetY = new Vector3( 0, Random.Range(UpperBoundY, LowerBoundY), 0);
		}

		if(good_item_generate_ticker > Good_Item_generate_frequency)
		{
			good_item_generate_ticker = 0;

			float t = good_item_position_switch_ticker / Good_Item_position_switch_frequency;

			float y = Vector3.Slerp(good_item_startY, good_item_targetY, t).y;

			int randomGameObject=Random.Range(0,numGoodGameObjects);
			GameObject goodItem = Instantiate(goodObjects[randomGameObject]) as GameObject;

			//Set y to be somewhere from UpperBoundY to LowerBoundY
			goodItem.transform.position = new Vector2(GenerationPositionX, y);

		}
	}

	public bool isGameOver = false;
	public void GameOver()
	{
		sceneFadeScript.EndScene ("FinalScene");
	}
}
