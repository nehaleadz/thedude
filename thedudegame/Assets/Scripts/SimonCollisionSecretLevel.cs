﻿using UnityEngine;
using System.Collections;

public class SimonCollisionSecretLevel : MonoBehaviour {

	public int Health = 1;
	public GUISkin guiSkin;
	
	private float hitTimeTicker = 0;
	public float hitTimer = 1.5f;

	private SceneFade sceneFadeScript;

	void Start() {
		sceneFadeScript = GameObject.FindObjectOfType (typeof(SceneFade)) as SceneFade;
	}

	void Update()
	{
		BlinkingSimon ();
	}
	
	void OnTriggerEnter2D( Collider2D item ) {
		AdjustHealth(-1);
		Destroy(item.gameObject);
	}

	void AdjustHealth(int addHealth)
	{
		Health += addHealth;
		
		isHit = true;
		
		if(Health <= 0 )
		{
			GameOver();
			Destroy(this.gameObject);
		}
	}
	
	private bool isHit = false;
	void BlinkingSimon()
	{
		if(isHit)
		{
			hitTimeTicker += Time.deltaTime;
			if(hitTimeTicker > hitTimer)
			{
				hitTimeTicker = 0;
				isHit = false;
			}
			float period = hitTimeTicker / hitTimer;
			period = period * 40;
			float heightRatio = 2/0.4f;
			float heightMoveUp = 0.6f;
			float colorManipulation = (Mathf.Sin (period) / heightRatio) + heightMoveUp;
			Color cc = this.renderer.material.color;
			this.renderer.material.color = new Color(cc.r, cc.g, cc.b, colorManipulation);
		}
		else
		{
			this.renderer.material.color = new Color(1f, 1f, 1f);
		}
	}
	
	void GameOver()
	{
		Debug.Log("GameOver");
		sceneFadeScript.EndScene ("FinalScene");
	}
}
