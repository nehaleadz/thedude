﻿using UnityEngine;
using System.Collections;

public class MoveBadGuys : MonoBehaviour {
	
	public float Speed = 5;
	public Vector3 direction=Vector3.left;
	
	// Update is called once per frame
	void Update () {
		float amtToMove = Speed * Time.deltaTime;
		transform.Translate (direction * amtToMove);
		
		if (transform.position.x < -38 || transform.position.x > 38) {
			Destroy(this.gameObject);
		}
	}
}
