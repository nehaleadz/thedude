﻿using UnityEngine;
using System.Collections;

public class HideShow : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.gameObject.collider2D.enabled=false;

		if (Input.GetButtonDown("Fire1") || Input.GetKeyDown("space")) 
		{
			transform.renderer.enabled=true;
			this.gameObject.collider2D.enabled=true;
		}
		else if (Input.GetButtonUp("Fire1") || Input.GetKeyUp("space")) 
		{
			transform.renderer.enabled=false;
		}
	}
}
