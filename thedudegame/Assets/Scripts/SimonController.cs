﻿using UnityEngine;
using System.Collections;

public class SimonController : MonoBehaviour {

	public Vector2 velocity=new Vector2(0,40);

	// Controls Simon jump
	private bool isGrounded = true;
	private bool isFirstJump = false;
	private bool isSecondJump = false;
	private Animator anim;

	private AudioSource[] _sounds;
	private AudioSource _jumpSound;

	void Start()
	{
		_sounds = GetComponents<AudioSource>();
		_jumpSound = _sounds [1];
		_jumpSound.enabled = true;
	}

	void Awake()
	{
		anim = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {

		if ((Input.GetButtonDown("Fire1") || Input.GetKeyDown("space")) && (isGrounded || isFirstJump)) {
			TryJump();
		}
	}

	// When Simon's on the ground, isGrounded is true
	void OnCollisionEnter2D( Collision2D ground ) {
		if (ground.gameObject.tag == "ground") {
			isGrounded = true;
			isFirstJump = false;
			isSecondJump = false;
			anim.SetTrigger("Land");
		}
	}

	// jump function
	private void TryJump() {


		if (isSecondJump) {
			isFirstJump = false;
			isSecondJump = false;
			return;
		}

		else {

			if (isGrounded) {
				isGrounded = false;
				isFirstJump = true;
				isSecondJump = false;
				Jump();
			}
			else {
				isGrounded = false;
				isFirstJump = false;
				isSecondJump = true;
				Jump();
			}
		}
	}

	private void Jump()
	{
		rigidbody2D.velocity = velocity;
		anim.SetTrigger("Jump");
		_jumpSound.Play ();
	}

}
