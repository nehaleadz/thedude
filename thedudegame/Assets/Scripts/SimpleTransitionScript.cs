﻿using UnityEngine;
using System.Collections;

public class SimpleTransitionScript : MonoBehaviour {

	public Vector3 StartPosition;
	public Vector3 FinalPosition;
	public float TimeLength;
	public bool MoveOnStart = true;
	public enum MoveTypeEnum
	{
		Lerp,
		Slerp
	};
	public MoveTypeEnum MoveType; 

	private bool _isMoving = false;
	private float _timeTicker = 0;

	// Use this for initialization
	void Start () {
		_isMoving = MoveOnStart;
	}
	
	// Update is called once per frame
	void Update () {
		if(_isMoving)
		{
			_timeTicker+=Time.deltaTime;
			if(_timeTicker > TimeLength)
			{
				_timeTicker = 0;
				_isMoving = false;
				return;
			}
			switch(MoveType)
			{
				case MoveTypeEnum.Lerp:
					transform.position = Vector3.Lerp (StartPosition, FinalPosition, _timeTicker / TimeLength);
					break;
				case MoveTypeEnum.Slerp:
					transform.position = Vector3.Slerp (StartPosition, FinalPosition, _timeTicker / TimeLength);
					break;
			}
		}
	}

	public void StartMoving()
	{
		_isMoving = true;
	}
}
